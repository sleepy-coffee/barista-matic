package com.sundance.barista.common.enums;

import static com.sundance.barista.common.utils.StringUtil.DRINK_PRICE_FORMATTER;
import static com.sundance.barista.common.utils.StringUtil.strToInt;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class StringTest {
	    
	    @Test
	    public void testStringUtil() {
	    	
	    	assertEquals(DRINK_PRICE_FORMATTER.format(0.344f), "$0.34");
	    	assertEquals(DRINK_PRICE_FORMATTER.format(0.345f), "$0.34");
	    	assertEquals(DRINK_PRICE_FORMATTER.format(0.346f), "$0.35");
	    	assertEquals(DRINK_PRICE_FORMATTER.format(12.344), "$12.34");
	    	assertEquals(DRINK_PRICE_FORMATTER.format(12.345), "$12.35");
	    	
	    	assertEquals(strToInt("-1"), -1);
	    	assertEquals(strToInt("0"), 0);
	    	assertEquals(strToInt("1"), 1);
	    	assertEquals(strToInt("12"), 12);
	    	assertEquals(strToInt("abc"), 0);
	    	
	    	
	    	assertEquals(strToInt("A", 16), 10);
	    	assertEquals(strToInt("B", 16), 11);
	    	assertEquals(strToInt("C", 16), 12);
	    	assertEquals(strToInt("D", 16), 13);
	    	assertEquals(strToInt("E", 16), 14);
	    	assertEquals(strToInt("F", 16), 15);
	    	assertEquals(strToInt("10", 16), 16);
	    	
	    }

	}

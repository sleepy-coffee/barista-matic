package com.sundance.barista.common.enums;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class EnumTest {

	    @Test
	    public void testCommandType() {
	    	
	    	assertEquals(CommandType.RESTOCK, CommandType.getType(0));
	    	assertEquals(CommandType.RESTOCK, CommandType.getType("R"));
	    	assertEquals(CommandType.RESTOCK, CommandType.getType("r"));
	    	
	    	assertEquals(CommandType.QUIT, CommandType.getType(1));
	    	assertEquals(CommandType.QUIT, CommandType.getType("Q"));
	    	assertEquals(CommandType.QUIT, CommandType.getType("q"));
	    	
	    	assertEquals(CommandType.UNSUPPORTED, CommandType.getType(-1));
	    	assertEquals(CommandType.UNSUPPORTED, CommandType.getType(2));
	    	assertEquals(CommandType.UNSUPPORTED, CommandType.getType("RR"));
	    	assertEquals(CommandType.UNSUPPORTED, CommandType.getType("rr"));
	    	
	    }

	    @Test
	    public void testHeaderType() {
	    	
	    	assertEquals(HeaderType.INVENTORY, HeaderType.getType(0));
	    	assertEquals(HeaderType.INVENTORY, HeaderType.getType("INVENTORY"));
	    	assertEquals(HeaderType.INVENTORY, HeaderType.getType("Inventory"));
	    	
	    	assertEquals(HeaderType.MENU, HeaderType.getType(1));
	    	assertEquals(HeaderType.MENU, HeaderType.getType("MENU"));
	    	assertEquals(HeaderType.MENU, HeaderType.getType("Menu"));
	    	
	    	assertEquals(HeaderType.UNSUPPORTED, HeaderType.getType(-1));
	    	assertEquals(HeaderType.UNSUPPORTED, HeaderType.getType(2));
	    	assertEquals(HeaderType.UNSUPPORTED, HeaderType.getType("INVENTORYINVENTORY"));
	    	assertEquals(HeaderType.UNSUPPORTED, HeaderType.getType("InventoryInventory"));
	    	
	    }

	    @Test
	    public void testIngredientType() {
	    	
	    	assertEquals(IngredientType.COFFEE, IngredientType.getType(0));
	    	assertEquals(IngredientType.COFFEE, IngredientType.getType("COFFEE"));
	    	assertEquals(IngredientType.COFFEE, IngredientType.getType("Coffee"));
	    	
	    	assertEquals(IngredientType.DECAF_COFFEE, IngredientType.getType(1));
	    	assertEquals(IngredientType.DECAF_COFFEE, IngredientType.getType("DECAF_COFFEE"));
	    	assertEquals(IngredientType.DECAF_COFFEE, IngredientType.getType("Decaf Coffee"));
	    	
	    	assertEquals(IngredientType.ESPRESSO, IngredientType.getType(2));
	    	assertEquals(IngredientType.ESPRESSO, IngredientType.getType("ESPRESSO"));
	    	assertEquals(IngredientType.ESPRESSO, IngredientType.getType("Espresso"));
	    	
	    	assertEquals(IngredientType.SUGAR, IngredientType.getType(10));
	    	assertEquals(IngredientType.SUGAR, IngredientType.getType("SUGAR"));
	    	assertEquals(IngredientType.SUGAR, IngredientType.getType("Sugar"));
	    	
	    	assertEquals(IngredientType.CREAM, IngredientType.getType(20));
	    	assertEquals(IngredientType.CREAM, IngredientType.getType("CREAM"));
	    	assertEquals(IngredientType.CREAM, IngredientType.getType("Cream"));
	    	
	    	assertEquals(IngredientType.WHIPPED_CREAM, IngredientType.getType(21));
	    	assertEquals(IngredientType.WHIPPED_CREAM, IngredientType.getType("WHIPPED_CREAM"));
	    	assertEquals(IngredientType.WHIPPED_CREAM, IngredientType.getType("Whipped Cream"));
	    	
	    	assertEquals(IngredientType.FOAMED_MILK, IngredientType.getType(30));
	    	assertEquals(IngredientType.FOAMED_MILK, IngredientType.getType("FOAMED_MILK"));
	    	assertEquals(IngredientType.FOAMED_MILK, IngredientType.getType("Foamed Milk"));
	    	
	    	assertEquals(IngredientType.STEAMED_MILK, IngredientType.getType(31));
	    	assertEquals(IngredientType.STEAMED_MILK, IngredientType.getType("STEAMED_MILK"));
	    	assertEquals(IngredientType.STEAMED_MILK, IngredientType.getType("Steamed Milk"));
	    	
	    	assertEquals(IngredientType.COCOA, IngredientType.getType(40));
	    	assertEquals(IngredientType.COCOA, IngredientType.getType("COCOA"));
	    	assertEquals(IngredientType.COCOA, IngredientType.getType("Cocoa"));
	    	
	    	assertEquals(IngredientType.UNSUPPORTED, IngredientType.getType(-1));
	    	assertEquals(IngredientType.UNSUPPORTED, IngredientType.getType("abcd"));
	    	
	    }
	    
	    
	    @Test
	    public void testMessageType() {
	    	
	    	assertEquals(MessageType.DISPENSING, MessageType.getType(0));
	    	assertEquals(MessageType.DISPENSING, MessageType.getType("DISPENSING"));
	    	assertEquals(MessageType.DISPENSING, MessageType.getType("Dispensing"));
	    	
	    	assertEquals(MessageType.OUT_OF_STOCK, MessageType.getType(10));
	    	assertEquals(MessageType.OUT_OF_STOCK, MessageType.getType("OUT_OF_STOCK"));
	    	assertEquals(MessageType.OUT_OF_STOCK, MessageType.getType("Out of Stock"));
	    	
	    	assertEquals(MessageType.INVALID_SELECTION, MessageType.getType(20));
	    	assertEquals(MessageType.INVALID_SELECTION, MessageType.getType("INVALID_SELECTION"));
	    	assertEquals(MessageType.INVALID_SELECTION, MessageType.getType("Invalid selection"));
	    	
	    	assertEquals(MessageType.UNSUPPORTED, MessageType.getType(-1));
	    	assertEquals(MessageType.UNSUPPORTED, MessageType.getType("abcd"));
	    	
	    }

	}

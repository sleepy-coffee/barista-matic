package com.sundance.barista.common.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public abstract class StringUtil {

	public static final String EMPTY_STRING = "";
	public static final String SPACE = " ";
	public static final String COLON = ":";
	public static final String TAB = "\t";
	public static final String DELIMITER = ",";

	public static final NumberFormat DRINK_PRICE_FORMATTER = new DecimalFormat("$#0.00"); 

	public static void output(String str) {

		if(str == null) {
			return;
		}

		System.out.println(str);

	}
	
	public static int strToInt(String str) {
		return strToInt(str, 10);
	}

	public static int strToInt(String str, int radix) {

		int value = 0;

		if(str == null || str.isEmpty() || str.toLowerCase().equals("null")) {
			return value;
		}

		try{
			value =  Integer.parseInt(str, radix);
		} catch(NumberFormatException e){
			
		}

		return value;
		
	}

}

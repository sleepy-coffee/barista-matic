package com.sundance.barista.common.enums;

import java.util.Arrays;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, or disclosed
 * without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public enum MessageType implements EnumInterface {

	DISPENSING("Dispensing" , 0),
	OUT_OF_STOCK("Out of Stock", 10),
	INVALID_SELECTION("Invalid selection", 20),
	
	UNSUPPORTED("Unsupported", -1),

	;

	private final String text;
	private final int intValue;

	private MessageType(String text, int intValue) {
		this.text = text;
		this.intValue = intValue;
	}

	public static MessageType getType(String text) {
		return text == null ? UNSUPPORTED : Arrays.asList(values()).stream().filter(
				type -> type.getText().equalsIgnoreCase(text.trim()) || type.toString().equalsIgnoreCase(text.trim())).
				findFirst().orElse(UNSUPPORTED);
	}

	public static MessageType getType(int intValue) {
		return Arrays.asList(values()).stream().filter(
				type -> type.getIntValue() == intValue).
				findFirst().orElse(UNSUPPORTED);
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the intValue
	 */
	public int getIntValue() {
		return intValue;
	}

}
package com.sundance.barista.common.enums;

import java.util.Arrays;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, or disclosed
 * without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public enum IngredientType implements EnumInterface {

	COFFEE("Coffee" , 0),
	DECAF_COFFEE("Decaf Coffee" , 1),
	ESPRESSO("Espresso" , 2),

	SUGAR("Sugar" , 10),

	CREAM("Cream" , 20),
	WHIPPED_CREAM("Whipped Cream" , 21),

	FOAMED_MILK("Foamed Milk" , 30),
	STEAMED_MILK("Steamed Milk" , 31),

	COCOA("Cocoa" , 40),

	UNSUPPORTED("Unsupported", -1),

	;

	private final String text;
	private final int intValue;

	private IngredientType(String text, int intValue) {
		this.text = text;
		this.intValue = intValue;
	}

	public static IngredientType getType(String text) {
		return text == null ? UNSUPPORTED : Arrays.asList(values()).stream().filter(
				type -> type.getText().equalsIgnoreCase(text.trim()) || 
				type.toString().equalsIgnoreCase(text.trim())).
				findFirst().orElse(UNSUPPORTED);
	}

	public static IngredientType getType(int intValue) {
		return Arrays.asList(values()).stream().filter(
				type -> type.getIntValue() == intValue).
				findFirst().orElse(UNSUPPORTED);
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the intValue
	 */
	public int getIntValue() {
		return intValue;
	}

}
Barista-matic Programming Assignment


# Barista-matic Programming Assignment using Java 8 and Spring Framework 5

This project was generated with apache maven version 3.6.1.

### Introduction

Creates a simulator of an automatic coffee dispensing machine, called the Barista- matic. The machine maintains an inventory of drink ingredients, and is able to dispense a fixed set of possible drinks by combining these ingredients in different amounts. The cost of a drink is determined by its component ingredients.

### How to start

In order to start the project use:

```bash
# The instruction below is for Mac or Linux users 
# Download and save the project on your local work station,   
# Open a terminal, enter the root directory of the project
$ cd barista-matic
# Use maven to clean, install the project's dependencies and build the application
$ mvn clean package
# Input the following command under the root directory of the project to start the application
$ java -jar barista-matic-main/target/barista-matic-main-1.0.0.jar
```

### Ingredient cost configuration

`barista-matic-main/src/main/resources/beans-barista-matic-ingredient-cost.xml` 

You can modify specific ingredient price by changing its corresponding value.

For example, if you want to change the cost of 'COFFEE' to $1.00, simple change this line

```xml
<entry key="COFFEE" value="0.75" />
```

to

```xml
<entry key="COFFEE" value="1.00" />
```

`barista-matic-common/src/main/java/com/sundance/barista/common/enums/com.sundance.barista.common.enums.IngredientType.java`

To add an additional ingredient to the coffee machine, you would need to add it to the java class as well as the xml configuration, then recompile the code

For example, if you want to add an ingredient such like 'Vanilla Extract', which costs $1.00 per portion. First you would need to add the following entry in the IngredientType class

```java
VANILLA_EXTRACT("Vanilla Extract", 50), 
```

then add its cost in beans-barista-matic-ingredient-cost.xml

```xml
<entry key="VANILLA_EXTRACT" value="1.00" />
```

make sure the value of key matches the corresponding enum name in IngredientType class. 

Recomplie the project once you are done with editing the ingredients

### Recipe configuration

`barista-matic-main/src/main/resources/beans-barista-matic-menu.xml` 

You can modify the menu (add / remove entries) by simply editing coffee beans and the list without modification the code.


### JUnit tests

In order to run unit test classes of the project use:

```bash
# The instruction below is for Mac or Linux users 
# Download and save the project on your local work station,   
# Open a terminal, enter the root directory of the project
$ cd barista-matic
# Use maven to clean, install the project's dependencies and build the application
$ mvn test

package com.sundance.barista.main;

import static com.sundance.barista.card.utils.CardUtil.calcCost;
import static com.sundance.barista.card.utils.CardUtil.displayCard;
import static com.sundance.barista.card.utils.CardUtil.restock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static com.sundance.barista.card.utils.CardUtil.dispenseCoffee;
import static com.sundance.barista.card.utils.CardUtil.MAX_INGREDIENT_UNITS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;

import com.sundance.barista.card.impl.Inventory;
import com.sundance.barista.card.impl.Menu;
import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class BaristaMaticMainTest {

	Inventory inventory;
	Menu menu;
	Map<IngredientType, Float> ingredientCostMap;
	float delta = 0.01f;

	@Before
	public void setup() {

		List<Drink> drinks = new ArrayList<>();

		String drinkName = "Coffee";
		Map<IngredientType, Integer> entries = new LinkedHashMap<>();
		entries.put(IngredientType.COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		drinks.add(new Drink(drinkName, entries));


		drinkName = "Decaf Coffee";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.DECAF_COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		drinks.add(new Drink(drinkName, entries));


		drinkName = "Caffe Latte";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 2);
		entries.put(IngredientType.STEAMED_MILK, 1);
		drinks.add(new Drink(drinkName, entries));

		drinkName = "Caffe Americano";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 3);
		drinks.add(new Drink(drinkName, entries));

		drinkName = "Caffe Mocha";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 1);
		entries.put(IngredientType.COCOA, 1);
		entries.put(IngredientType.STEAMED_MILK, 1);
		entries.put(IngredientType.WHIPPED_CREAM, 1);
		drinks.add(new Drink(drinkName, entries));

		// Test Caffe Mocha"
		drinkName = "Cappuccino";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 2);
		entries.put(IngredientType.STEAMED_MILK, 1);
		entries.put(IngredientType.FOAMED_MILK, 1);
		drinks.add(new Drink(drinkName, entries));

		ingredientCostMap = new HashMap<>();
		ingredientCostMap.put(IngredientType.COFFEE, 0.75f);
		ingredientCostMap.put(IngredientType.DECAF_COFFEE, 0.75f);
		ingredientCostMap.put(IngredientType.ESPRESSO, 1.10f);

		ingredientCostMap.put(IngredientType.SUGAR, 0.25f);

		ingredientCostMap.put(IngredientType.CREAM, 0.25f);
		ingredientCostMap.put(IngredientType.WHIPPED_CREAM, 1.00f);

		ingredientCostMap.put(IngredientType.FOAMED_MILK, 0.35f);
		ingredientCostMap.put(IngredientType.STEAMED_MILK, 0.35f);

		ingredientCostMap.put(IngredientType.COCOA, 0.90f);

		drinks.forEach(drink -> calcCost(drink, ingredientCostMap));

		menu = new Menu(drinks);

		inventory = new Inventory(ingredientCostMap.keySet());

	}

	// Create a drink (Decaf Coffee) manually
	@Test
	public void tesetDispenseCoffees() {

		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

		testDispenseCaffeAmericano();

		testDispenseCaffeLatte();

		testDispenseCaffeMocha();

		testDispenseCappuccino();

		testDispenseCoffee();

		testDispenseDecafCoffee();

		testDispenseAssortedmCoffees();

	}


	public void testDispenseCaffeAmericano() {

		assertEquals(menu.getEntries().get("Caffe Americano").getCost(), 3.30f, delta);

		IntStream.range(0, 3).boxed().forEach(i -> {
			assertTrue(menu.isInStock("Caffe Americano"));
			dispenseCoffee(inventory, menu, "1");
		});
		assertFalse(menu.isInStock("Caffe Americano"));
		assertFalse(menu.isInStock("Caffe Latte"));
		assertTrue(menu.isInStock("Caffe Mocha"));
		assertFalse(menu.isInStock("Cappuccino"));
		assertTrue(menu.isInStock("Coffee"));
		assertTrue(menu.isInStock("Decaf Coffee"));
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 1);
		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

	}

	public void testDispenseCaffeLatte() {

		assertEquals(menu.getEntries().get("Caffe Latte").getCost(), 2.55f, delta);

		IntStream.range(0, 5).boxed().forEach(i -> {
			assertTrue(menu.isInStock("Caffe Latte"));
			dispenseCoffee(inventory, menu, "2");
		});
		assertFalse(menu.isInStock("Caffe Americano"));
		assertFalse(menu.isInStock("Caffe Latte"));
		assertFalse(menu.isInStock("Caffe Mocha"));
		assertFalse(menu.isInStock("Cappuccino"));
		assertTrue(menu.isInStock("Coffee"));
		assertTrue(menu.isInStock("Decaf Coffee"));
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 0);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 5);
		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

	}

	public void testDispenseCaffeMocha() {

		assertEquals(menu.getEntries().get("Caffe Mocha").getCost(), 3.35f, delta);

		IntStream.range(0, 10).boxed().forEach(i -> {
			assertTrue(menu.isInStock("Caffe Mocha"));
			dispenseCoffee(inventory, menu, "3");
		});
		assertFalse(menu.isInStock("Caffe Americano"));
		assertFalse(menu.isInStock("Caffe Latte"));
		assertFalse(menu.isInStock("Caffe Mocha"));
		assertFalse(menu.isInStock("Cappuccino"));
		assertTrue(menu.isInStock("Coffee"));
		assertTrue(menu.isInStock("Decaf Coffee"));
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 0);
		assertEquals(inventory.getEntries().get(IngredientType.COCOA).intValue(), 0);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 0);
		assertEquals(inventory.getEntries().get(IngredientType.WHIPPED_CREAM).intValue(), 0);
		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

	}

	public void testDispenseCappuccino() {

		assertEquals(menu.getEntries().get("Cappuccino").getCost(), 2.90f, delta);

		IntStream.range(0, 5).boxed().forEach(i -> {
			assertTrue(menu.isInStock("Cappuccino"));
			dispenseCoffee(inventory, menu, "4");
		});
		assertFalse(menu.isInStock("Caffe Americano"));
		assertFalse(menu.isInStock("Caffe Latte"));
		assertFalse(menu.isInStock("Caffe Mocha"));
		assertFalse(menu.isInStock("Cappuccino"));
		assertTrue(menu.isInStock("Coffee"));
		assertTrue(menu.isInStock("Decaf Coffee"));
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 0);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 5);
		assertEquals(inventory.getEntries().get(IngredientType.FOAMED_MILK).intValue(), 5);
		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

	}

	public void testDispenseCoffee() {

		assertEquals(menu.getEntries().get("Coffee").getCost(), 2.75f, delta);

		IntStream.range(0, 3).boxed().forEach(i -> {
			assertTrue(menu.isInStock("Coffee"));
			dispenseCoffee(inventory, menu, "5");
		});
		assertTrue(menu.isInStock("Caffe Americano"));
		assertTrue(menu.isInStock("Caffe Latte"));
		assertTrue(menu.isInStock("Caffe Mocha"));
		assertTrue(menu.isInStock("Cappuccino"));
		assertFalse(menu.isInStock("Coffee"));
		assertTrue(menu.isInStock("Decaf Coffee"));
		assertEquals(inventory.getEntries().get(IngredientType.COFFEE).intValue(), 1);
		assertEquals(inventory.getEntries().get(IngredientType.SUGAR).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.CREAM).intValue(), 7);
		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

	}

	public void testDispenseDecafCoffee() {

		assertEquals(menu.getEntries().get("Decaf Coffee").getCost(), 2.75f, delta);

		IntStream.range(0, 3).boxed().forEach(i -> {
			assertTrue(menu.isInStock("Decaf Coffee"));
			dispenseCoffee(inventory, menu, "6");
		});
		assertTrue(menu.isInStock("Caffe Americano"));
		assertTrue(menu.isInStock("Caffe Latte"));
		assertTrue(menu.isInStock("Caffe Mocha"));
		assertTrue(menu.isInStock("Cappuccino"));
		assertTrue(menu.isInStock("Coffee"));
		assertFalse(menu.isInStock("Decaf Coffee"));
		assertEquals(inventory.getEntries().get(IngredientType.DECAF_COFFEE).intValue(), 1);
		assertEquals(inventory.getEntries().get(IngredientType.SUGAR).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.CREAM).intValue(), 7);
		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));

	}

	public void testDispenseAssortedmCoffees() {


		assertTrue(menu.isInStock("Caffe Americano"));
		dispenseCoffee(inventory, menu, "1");

		assertTrue(menu.isInStock("Caffe Latte"));
		dispenseCoffee(inventory, menu, "2");

		assertTrue(menu.isInStock("Caffe Mocha"));
		dispenseCoffee(inventory, menu, "3");

		assertTrue(menu.isInStock("Cappuccino"));
		dispenseCoffee(inventory, menu, "4");

		assertTrue(menu.isInStock("Coffee"));
		dispenseCoffee(inventory, menu, "5");

		assertTrue(menu.isInStock("Decaf Coffee"));
		dispenseCoffee(inventory, menu, "6");

		assertFalse(menu.isInStock("Caffe Americano"));
		assertTrue(menu.isInStock("Caffe Latte"));
		assertTrue(menu.isInStock("Caffe Mocha"));
		assertTrue(menu.isInStock("Cappuccino"));
		assertTrue(menu.isInStock("Coffee"));
		assertTrue(menu.isInStock("Decaf Coffee"));

		assertEquals(inventory.getEntries().get(IngredientType.COFFEE).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.DECAF_COFFEE).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 2);
		assertEquals(inventory.getEntries().get(IngredientType.SUGAR).intValue(), 8);
		assertEquals(inventory.getEntries().get(IngredientType.CREAM).intValue(), 8);
		assertEquals(inventory.getEntries().get(IngredientType.WHIPPED_CREAM).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.FOAMED_MILK).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.COCOA).intValue(), 9);

		displayCard(inventory, menu);

		restock(inventory, menu);
		displayCard(inventory, menu);
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));
		inventory.getEntries().values().forEach(value -> assertEquals(value.intValue(), MAX_INGREDIENT_UNITS));

	}

}

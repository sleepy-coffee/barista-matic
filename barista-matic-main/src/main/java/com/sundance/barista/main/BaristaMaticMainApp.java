package com.sundance.barista.main;

import static com.sundance.barista.card.utils.CardUtil.calcCost;
import static com.sundance.barista.card.utils.CardUtil.dispenseCoffee;
import static com.sundance.barista.card.utils.CardUtil.displayCard;
import static com.sundance.barista.card.utils.CardUtil.displayMessage;
import static com.sundance.barista.card.utils.CardUtil.restock;
import static com.sundance.barista.common.utils.StringUtil.EMPTY_STRING;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import com.sundance.barista.card.impl.AbstractCard;
import com.sundance.barista.card.impl.Inventory;
import com.sundance.barista.card.impl.Menu;
import com.sundance.barista.common.enums.CommandType;
import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.common.enums.MessageType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class BaristaMaticMainApp {

	protected Map<IngredientType, Float> ingredientCostMap;

	protected List<Drink> drinkList;

	protected AbstractCard<?, ?> inventory;

	protected AbstractCard<?, ?> menu;

	public boolean init() {

		inventory = new Inventory(ingredientCostMap.keySet());

		drinkList.forEach(drink -> calcCost(drink, ingredientCostMap));

		menu = new Menu(drinkList);

		return true;

	}

	public void start() {

		displayCard(inventory, menu);

		boolean done = false;
		BufferedReader br = new BufferedReader(new InputStreamReader(
				System.in));

		while (!done) {

			String line = EMPTY_STRING;
			while (line.isEmpty()) {
				try {
					line = br.readLine();
				} catch (Exception e) {

				}
			}

			if(line.length() > 1) {
				displayMessage(MessageType.INVALID_SELECTION, line);
				displayCard(inventory, menu);
				continue;
			}

			/*
			 * Each valid command consists of a single character
			 *  'R' or 'r' - restock the inventory and redisplay the menu
			 *	'Q' or 'q' - quit the application
			 *	[1-6] - order the drink with the corresponding number in the menu
			 */
			switch (CommandType.getType(line)) {
			case QUIT:
				done = true;
				break;
			case RESTOCK:	
				restock(inventory, menu);
				displayCard(inventory, menu);
				break;
			default:
				if(!dispenseCoffee(inventory, menu, line)) {
					displayMessage(MessageType.INVALID_SELECTION, line);
				}
				displayCard(inventory, menu);
				break;
			}
		}

		if(br != null) {
			try {
				br.close();
			} catch (Exception e) {

			}
		}

	}

	/**
	 * Setters && Getters
	 */
	/**
	 * @param ingredientCostMap the ingredientCostMap to set
	 */
	public void setIngredientCostMap(Map<IngredientType, Float> ingredientCostMap) {
		this.ingredientCostMap = ingredientCostMap;
	}

	/**
	 * @param drinkMap the drinkMap to set
	 */
	public void setDrinkList(List<Drink> drinkList) {
		this.drinkList = drinkList;
	}

}


package com.sundance.barista.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class BaristaMaticMain {

	public static void main( String[] args ) {

		ApplicationContext context = new ClassPathXmlApplicationContext("beans-barista-matic-main.xml");
		BaristaMaticMainApp mainApp = (BaristaMaticMainApp) context.getBean("mainApp");

		if(mainApp.init()) {
			mainApp.start();
		}

		((ConfigurableApplicationContext) context).close();				

	}

}


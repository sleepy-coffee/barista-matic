package com.sundance.barista.map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.LinkedHashMap;
import java.util.Map;

import org.junit.Test;

import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class DrinkTest {

	@Test
	public void testDrink() {

		String drinkName = "Coffee"; 
		float drinkCost = 2.75f;
		float delta = 0.01f;

		Drink drink = new Drink();
		drink.setName(drinkName);
		drink.setCost(drinkCost);

		Map<IngredientType, Integer> entries = new LinkedHashMap<>();
		entries.put(IngredientType.COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		drink.setEntries(entries);

		assertEquals(drink.getName(), drinkName);
		assertEquals(drink.getCost(), drinkCost, delta);

		assertSame(drink.getEntries(), entries);
		assertEquals(drink.getEntries().get(IngredientType.COFFEE).intValue(), 3);
		assertEquals(drink.getEntries().get(IngredientType.SUGAR).intValue(), 1);
		assertEquals(drink.getEntries().get(IngredientType.CREAM).intValue(), 1);

	}

}

package com.sundance.barista.map.impl;

import java.util.Map;

import com.sundance.barista.common.enums.IngredientType;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, or disclosed
 * without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class Drink extends AbstractEntryMap<IngredientType, Integer> {

	protected String name;

	protected float cost;

	/**
	 * 
	 */
	public Drink() {
		super();
	}

	/**
	 * @param name
	 */
	public Drink(String name) {
		this.name = name;
	}

	public Drink(Map<IngredientType, Integer> entries) {
		super(entries);
	}

	/**
	 * @param name
	 * @param ingredients
	 */
	public Drink(String name, Map<IngredientType, Integer> entries) {
		this(entries);
		this.name = name;
	}

	/**
	 * Setters && Getters
	 */
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cost
	 */
	public float getCost() {
		return cost;
	}

	/**
	 * @param cost the cost to set
	 */
	public void setCost(float cost) {
		this.cost = cost;
	}

}
package com.sundance.barista.map;

import java.util.Map;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public interface EntryMapInterface<T, S> {

	/**
	 * Setters & Getters
	 */
	/**
	 * @return the entries
	 */
	public Map<T, S> getEntries();

	/**
	 * @param entries the entries to set
	 */
	public void setEntries(Map<T, S> entries);

}
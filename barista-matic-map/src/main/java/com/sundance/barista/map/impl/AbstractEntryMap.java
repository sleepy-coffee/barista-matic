package com.sundance.barista.map.impl;

import java.util.HashMap;
import java.util.Map;

import com.sundance.barista.map.EntryMapInterface;


/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public abstract class AbstractEntryMap<T, S> implements EntryMapInterface<T, S> {

	protected Map<T, S> entries;

	/**
	 * 
	 */
	public AbstractEntryMap() {
		super();
		entries = new HashMap<>();
	}

	/**
	 * @param ingredients
	 */
	public AbstractEntryMap(Map<T, S> entries) {
		this();
		this.entries = entries;
	}

	/**
	 * Setters & Getters
	 */
	/**
	 * @return the entries
	 */
	public Map<T, S> getEntries() {
		return entries;
	}

	/**
	 * @param entries the entries to set
	 */
	public void setEntries(Map<T, S> entries) {
		this.entries = entries;
	}

}
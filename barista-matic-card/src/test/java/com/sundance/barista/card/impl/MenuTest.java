package com.sundance.barista.card.impl;

import static com.sundance.barista.card.utils.CardUtil.calcCost;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class MenuTest {

	Menu menu;
	Map<IngredientType, Float> ingredientCostMap;
	
	@Before
	public void setup() {

		List<Drink> drinks = new ArrayList<>();

		String drinkName = "Coffee";
		Map<IngredientType, Integer> entries = new LinkedHashMap<>();
		entries.put(IngredientType.COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		drinks.add(new Drink(drinkName, entries));


		drinkName = "Decaf Coffee";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.DECAF_COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		drinks.add(new Drink(drinkName, entries));


		drinkName = "Caffe Latte";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 2);
		entries.put(IngredientType.STEAMED_MILK, 1);
		drinks.add(new Drink(drinkName, entries));

		drinkName = "Caffe Americano";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 3);
		drinks.add(new Drink(drinkName, entries));

		drinkName = "Caffe Mocha";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 1);
		entries.put(IngredientType.COCOA, 1);
		entries.put(IngredientType.STEAMED_MILK, 1);
		entries.put(IngredientType.WHIPPED_CREAM, 1);
		drinks.add(new Drink(drinkName, entries));

		// Test Caffe Mocha"
		drinkName = "Cappuccino";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 2);
		entries.put(IngredientType.STEAMED_MILK, 1);
		entries.put(IngredientType.FOAMED_MILK, 1);
		drinks.add(new Drink(drinkName, entries));

		ingredientCostMap = new HashMap<>();
		ingredientCostMap.put(IngredientType.COFFEE, 0.75f);
		ingredientCostMap.put(IngredientType.DECAF_COFFEE, 0.75f);
		ingredientCostMap.put(IngredientType.ESPRESSO, 1.10f);
		
		ingredientCostMap.put(IngredientType.SUGAR, 0.25f);
		
		ingredientCostMap.put(IngredientType.CREAM, 0.25f);
		ingredientCostMap.put(IngredientType.WHIPPED_CREAM, 1.00f);
		
		ingredientCostMap.put(IngredientType.FOAMED_MILK, 0.35f);
		ingredientCostMap.put(IngredientType.STEAMED_MILK, 0.35f);
		
		ingredientCostMap.put(IngredientType.COCOA, 0.90f);

		drinks.forEach(drink -> calcCost(drink, ingredientCostMap));
		
		menu = new Menu(drinks);
		
	}

	@Test
	public void testMenu() {

		menu.display();
		
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));
		menu.getEntries().keySet().forEach(name -> menu.setInStock(name, false));
		menu.getEntries().keySet().forEach(name -> assertFalse(menu.isInStock(name)));
		menu.restock();
		menu.getEntries().keySet().forEach(name -> assertTrue(menu.isInStock(name)));
		
	}

}

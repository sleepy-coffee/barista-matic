package com.sundance.barista.card.impl;

import static com.sundance.barista.card.utils.CardUtil.MAX_INGREDIENT_UNITS;
import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;

import com.sundance.barista.common.enums.HeaderType;
import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class InventoryTest {

	Inventory inventory = null;

	@Before
	public void setup() {

		Set<IngredientType> set = new HashSet<>();
		set.add(IngredientType.COFFEE);
		set.add(IngredientType.DECAF_COFFEE);
		set.add(IngredientType.ESPRESSO);
		set.add(IngredientType.SUGAR);
		set.add(IngredientType.CREAM);
		set.add(IngredientType.WHIPPED_CREAM);
		set.add(IngredientType.FOAMED_MILK);
		set.add(IngredientType.STEAMED_MILK);
		set.add(IngredientType.COCOA);

		inventory = new Inventory(set);

	}

	@Test
	public void testInventory() {

		inventory.display();
		assertEquals(inventory.getHeaderType(), HeaderType.INVENTORY);
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		// Test Coffee
		String drinkName = "Coffee";
		Map<IngredientType, Integer> entries = new HashMap<>();
		entries.put(IngredientType.COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		Drink drink = new Drink(drinkName, entries);

		inventory.takeOutOfStock(drink);
		assertEquals(inventory.getEntries().get(IngredientType.COFFEE).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.SUGAR).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.CREAM).intValue(), 9);
		inventory.getEntries().entrySet().stream().filter(entry -> 
		entry.getKey() != IngredientType.COFFEE && 
		entry.getKey() != IngredientType.SUGAR && 
		entry.getKey() != IngredientType.CREAM)
		.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
		.values().forEach(count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		inventory.restock();
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		// Test Decaf Coffee
		drinkName = "Decaf Coffee";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.DECAF_COFFEE, 3);
		entries.put(IngredientType.SUGAR, 1);
		entries.put(IngredientType.CREAM, 1);
		drink = new Drink(drinkName, entries);

		inventory.takeOutOfStock(drink);
		assertEquals(inventory.getEntries().get(IngredientType.DECAF_COFFEE).intValue(), 7);
		assertEquals(inventory.getEntries().get(IngredientType.SUGAR).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.CREAM).intValue(), 9);
		inventory.getEntries().entrySet().stream().filter(entry -> 
		entry.getKey() != IngredientType.DECAF_COFFEE && 
		entry.getKey() != IngredientType.SUGAR && 
		entry.getKey() != IngredientType.CREAM)
		.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
		.values().forEach(count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		inventory.restock();
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		// Test Caffe Latte"
		drinkName = "Caffe Latte";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 2);
		entries.put(IngredientType.STEAMED_MILK, 1);
		drink = new Drink(drinkName, entries);

		inventory.takeOutOfStock(drink);
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 8);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 9);
		inventory.getEntries().entrySet().stream().filter(entry -> 
		entry.getKey() != IngredientType.ESPRESSO && 
		entry.getKey() != IngredientType.STEAMED_MILK)
		.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
		.values().forEach(count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		inventory.restock();
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		// Test Caffe Americano"
		drinkName = "Caffe Americano";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 3);
		drink = new Drink(drinkName, entries);

		inventory.takeOutOfStock(drink);
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 7);
		inventory.getEntries().entrySet().stream().filter(entry -> 
		entry.getKey() != IngredientType.ESPRESSO)
		.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
		.values().forEach(count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		inventory.restock();
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));


		// Test Caffe Mocha"
		drinkName = "Caffe Mocha";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 1);
		entries.put(IngredientType.COCOA, 1);
		entries.put(IngredientType.STEAMED_MILK, 1);
		entries.put(IngredientType.WHIPPED_CREAM, 1);
		drink = new Drink(drinkName, entries);

		inventory.takeOutOfStock(drink);
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.COCOA).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.WHIPPED_CREAM).intValue(), 9);
		inventory.getEntries().entrySet().stream().filter(entry -> 
		entry.getKey() != IngredientType.ESPRESSO &&
		entry.getKey() != IngredientType.COCOA &&
		entry.getKey() != IngredientType.STEAMED_MILK &&
		entry.getKey() != IngredientType.WHIPPED_CREAM)
		.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
		.values().forEach(count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		inventory.restock();
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));


		// Test Caffe Mocha"
		drinkName = "Cappuccino";
		entries = new LinkedHashMap<>();
		entries.put(IngredientType.ESPRESSO, 2);
		entries.put(IngredientType.STEAMED_MILK, 1);
		entries.put(IngredientType.FOAMED_MILK, 1);
		drink = new Drink(drinkName, entries);

		inventory.takeOutOfStock(drink);
		assertEquals(inventory.getEntries().get(IngredientType.ESPRESSO).intValue(), 8);
		assertEquals(inventory.getEntries().get(IngredientType.STEAMED_MILK).intValue(), 9);
		assertEquals(inventory.getEntries().get(IngredientType.FOAMED_MILK).intValue(), 9);
		inventory.getEntries().entrySet().stream().filter(entry -> 
		entry.getKey() != IngredientType.ESPRESSO &&
		entry.getKey() != IngredientType.STEAMED_MILK &&
		entry.getKey() != IngredientType.FOAMED_MILK)
		.collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()))
		.values().forEach(count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

		inventory.restock();
		inventory.getEntries().values().forEach(
				count -> assertEquals(count.intValue(), MAX_INGREDIENT_UNITS));

	}

}

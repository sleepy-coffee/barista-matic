package com.sundance.barista.card.utils;

import static com.sundance.barista.common.utils.StringUtil.COLON;
import static com.sundance.barista.common.utils.StringUtil.SPACE;
import static com.sundance.barista.common.utils.StringUtil.output;
import static com.sundance.barista.common.utils.StringUtil.strToInt;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

import com.sundance.barista.card.impl.AbstractCard;
import com.sundance.barista.card.impl.Inventory;
import com.sundance.barista.card.impl.Menu;
import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.common.enums.MessageType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public abstract class CardUtil {

	public static final int MAX_INGREDIENT_UNITS = 10;

	public static void displayMessage(MessageType type, String str) {
		output(type.getText() + COLON + SPACE + str);
	}

	public static void displayCard(AbstractCard<?, ?> ... cards) {

		if(cards == null || cards.length < 1) {
			return;
		}

		Arrays.asList(cards).forEach(card -> card.display());

	}

	public static void restock(AbstractCard<?, ?> ... cards) {

		if(cards == null || cards.length < 1) {
			return;
		}

		Arrays.asList(cards).forEach(card -> card.restock());

	}

	public static boolean dispenseCoffee(AbstractCard<?, ?> inventory, 
			AbstractCard<?, ?> menu, String command) {
		return dispenseCoffee(inventory, menu, getOrderIndex(command));
	}

	/**
	 * Dispense coffee if it is in stock, otherwise display out of stock message
	 * 1. If the coffee is in stock, 
	 * 	a. Display despensing coffee message
	 * 	b. Take corresponding ingredients out of stock
	 * 	c. Reset all drinks availabilities
	 * 2. If the coffee is out of stock
	 * 	display out of stock message
	 * 
	 * @param inventory inventory of all ingredients and their amounts
	 * @param menu menu of all drinks and their availabilities
	 * @param index the index of the drink to dispense, starting from 0, which is offset by one from user input
	 * @return true if it the requested drink in stock, otherwise false
	 */
	public static boolean dispenseCoffee(AbstractCard<?, ?> inventory, 
			AbstractCard<?, ?> menu, int index) {

		if(inventory == null || menu == null) {
			return false;
		}

		@SuppressWarnings("unchecked")
		Map<String, Drink> coffees = (Map<String, Drink>) menu.getEntries();
		if(coffees == null || coffees.isEmpty() || index >= coffees.size() || index < 0) {
			return false;
		}

		String name = coffees.keySet().stream()
				.sorted().collect(Collectors.toList()).get(index);

		if(((Menu) menu).isInStock(name)) {
			displayMessage(MessageType.DISPENSING, name);
			((Inventory) inventory).takeOutOfStock(coffees.get(name));
			coffees.entrySet().stream().forEach(entry -> 
			((Menu) menu).setInStock(entry.getKey(), isInStock((Inventory) inventory, entry.getValue())));
		} else {
			displayMessage(MessageType.OUT_OF_STOCK, name);
		}

		return true;

	}

	/**
	 * @param inventory inventory of all ingredients and their amounts
	 * @param drink the drink to be checked
	 * @return true if the requested drink is in stock, otherwise false
	 */
	protected static boolean isInStock(Inventory inventory, Drink drink) {

		if(inventory == null || drink == null) {
			return false;
		}

		Map<IngredientType, Integer> entries = inventory.getEntries();
		if(entries == null || entries.isEmpty()) {
			return false;
		}

		Map<IngredientType, Integer> ingredients = drink.getEntries();
		if(ingredients == null || ingredients.isEmpty()) {
			return false;
		}

		for(Map.Entry<IngredientType, Integer> entry: ingredients.entrySet()) {
			if(entries.containsKey(entry.getKey())) {
				if(entries.get(entry.getKey()) < entry.getValue()) {
					return false;
				}
			}
		}

		return true;

	}

	/**
	 * Calculate the cost of the drink by adding the costs of all ingredients
	 * @param drink the drink to be checked
	 * @param ingredientCostMap a hash map contains costs of all ingredients
	 */
	public static void calcCost(Drink drink, Map<IngredientType, Float> ingredientCostMap) {

		if(drink == null ||	ingredientCostMap == null || ingredientCostMap.isEmpty()) {
			return;
		}

		Map<IngredientType, Integer> ingredients = drink.getEntries();
		if(ingredients == null ||ingredients.isEmpty()) {
			return;
		}

		float cost = 0.f;
		for(IngredientType type: ingredients.keySet()) {
			if(ingredientCostMap.containsKey(type)) {
				cost += ingredients.get(type) * ingredientCostMap.get(type);
			}
		}

		drink.setCost(cost);

	}

	/**
	 * Parse the index of the requested drink and offset it by one
	 * @param command the command to order coffee
	 * @return the index of the requested drink in the drinks linked hash map
	 */
	protected static int getOrderIndex(String command) {
		return strToInt(command) - 1;
	}

}

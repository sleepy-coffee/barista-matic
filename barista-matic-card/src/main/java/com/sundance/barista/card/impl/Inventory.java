package com.sundance.barista.card.impl;

import static com.sundance.barista.card.utils.CardUtil.MAX_INGREDIENT_UNITS;
import static com.sundance.barista.common.utils.StringUtil.DELIMITER;
import static com.sundance.barista.common.utils.StringUtil.output;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.sundance.barista.common.enums.HeaderType;
import com.sundance.barista.common.enums.IngredientType;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class Inventory extends AbstractCard<IngredientType, Integer> {

	/**
	 * 
	 */
	public Inventory() {
		super(HeaderType.INVENTORY);
	}

	/**
	 * @param ingredients
	 */
	public Inventory(Map<IngredientType, Integer> entries) {

		super(HeaderType.INVENTORY, entries);

		if(entries != null && !entries.isEmpty()) {
			restock(entries.keySet());
		}

	}

	/**
	 * @param ingredients
	 */
	public Inventory(Set<IngredientType> ingredients) {
		this();
		restock(ingredients);
	}

	/**
	 * @param ingredients
	 */
	public Inventory(List<IngredientType> ingredients) {
		this();
		restock(ingredients);
	}

	public void restock(Set<IngredientType> ingredients) {

		if(ingredients == null || ingredients.isEmpty()) {
			return;
		}

		restock(ingredients.stream().collect(Collectors.toList()));

	}

	/**
	 * Restock the inventory by setting all ingredient amount to the maximus value, 
	 * then sort the ingredients by their names in a linked hash map
	 * @param ingredients
	 */
	public void restock(List<IngredientType> ingredients) {

		if(ingredients == null || ingredients.isEmpty()) {
			return;
		}

		entries = ingredients.stream().sorted(Comparator.comparing(IngredientType::toString))
				.collect(Collectors.toMap(type -> type, type -> MAX_INGREDIENT_UNITS,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

	}

	/**
	 * Restock the inventory by setting all ingredient amount to the maximus value, 
	 */
	public void restock() {

		if(entries == null || entries.isEmpty()) {
			return;
		}

		entries.entrySet().stream().forEach(entry -> entry.setValue(MAX_INGREDIENT_UNITS));

	}

	public boolean takeOutOfStock(Drink drink) {

		if(drink == null || entries == null || entries.isEmpty()) {
			return false;
		}

		Map<IngredientType, Integer> ingredients = drink.getEntries();
		if(ingredients == null || ingredients.isEmpty()) {
			return false;
		}

		for(Map.Entry<IngredientType, Integer> entry: ingredients.entrySet()) {
			if(entries.containsKey(entry.getKey())) {
				entries.put(entry.getKey(), entries.get(entry.getKey()) - entry.getValue());
			}
		}

		return true;

	}

	@Override
	public void display() {

		super.display();

		if(entries == null || entries.isEmpty()) {
			return;
		}

		entries.entrySet().stream().forEach(entry -> 
		output(entry.getKey().getText() + DELIMITER + entry.getValue()));

	}

}
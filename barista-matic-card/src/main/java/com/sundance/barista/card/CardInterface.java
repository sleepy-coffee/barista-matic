package com.sundance.barista.card;

import com.sundance.barista.common.enums.HeaderType;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public interface CardInterface {

	public void display();
	
	/**
	 * Setters & Getters
	 */
	/**
	 * @return the type
	 */
	public HeaderType getHeaderType();

	/**
	 * @param type the type to set
	 */
	public void setHeaderType(HeaderType type);


}
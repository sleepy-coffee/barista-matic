package com.sundance.barista.card.impl;

import static com.sundance.barista.common.utils.StringUtil.COLON;
import static com.sundance.barista.common.utils.StringUtil.output;

import java.util.Map;

import com.sundance.barista.card.CardInterface;
import com.sundance.barista.common.enums.HeaderType;
import com.sundance.barista.map.impl.AbstractEntryMap;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public abstract class AbstractCard<T, S> extends AbstractEntryMap<T, S> implements CardInterface {

	protected HeaderType headerType;

	public abstract void restock();
	
	/**
	 * 
	 */
	public AbstractCard() {
		super();
	}

	/**
	 * @param headerType
	 */
	public AbstractCard(HeaderType headerType) {
		super();
		this.headerType = headerType;
	}

	/**
	 * @param ingredients
	 */
	public AbstractCard(Map<T, S> entries) {
		super(entries);
	}
	
	/**
	 * @param ingredients
	 */
	public AbstractCard(HeaderType headerType, Map<T, S> entries) {
		this(entries);
		this.headerType = headerType;
	}

	public void display() {

		if(headerType == null || headerType == HeaderType.UNSUPPORTED) {
			return;
		}

		output(headerType.getText() + COLON);
		
	}

	/**
	 * Setters & Getters
	 */
	/**
	 * @return the headerType
	 */
	public HeaderType getHeaderType() {
		return headerType;
	}

	/**
	 * @param headerType the headerType to set
	 */
	public void setHeaderType(HeaderType headerType) {
		this.headerType = headerType;
	}

}
package com.sundance.barista.card.impl;

import static com.sundance.barista.common.utils.StringUtil.DELIMITER;
import static com.sundance.barista.common.utils.StringUtil.output;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.sundance.barista.common.enums.HeaderType;
import com.sundance.barista.common.utils.StringUtil;
import com.sundance.barista.map.impl.Drink;

/*******************************************************************************
 *
 * Copyright 2020, Sundance Studio.
 *
 * This file contains confidential and proprietary information that is the
 * property of Sundance Studio. This information may not be used, copied, 
 * or disclosed without prior authorization by Sundance Studio.
 *
 *
 * @author 	Wei Zhou wzhoumadison@gmail.com
 * 
 * @version 1.0.0 
 * @Since   2020-03-15
 * 
 ******************************************************************************/

public class Menu extends AbstractCard<String, Drink> {

	protected Map<String, Boolean> stockMap;

	/**
	 * 
	 */
	public Menu() {
		super(HeaderType.MENU);
	}

	/**
	 * Initialize the menu by setting all drinks in stock , 
	 * then sort the drinks by their names in a linked hash map
	 * @param ingredients
	 */
	public Menu(List<Drink> drinks) {

		this();

		if(drinks == null || drinks.isEmpty()) {
			return;
		}

		entries = drinks.stream().sorted(Comparator.comparing(Drink::getName))
				.collect(Collectors.toMap(drink -> drink.getName(), drink -> drink,
						(oldValue, newValue) -> oldValue, LinkedHashMap::new));

		stockMap = drinks.stream().collect(
				Collectors.toMap(drink -> drink.getName(), drink -> true));

	}

	/**
	 * Restock the menu by setting all drinks in stock 
	 */
	public void restock() {

		if(stockMap == null || stockMap.isEmpty()) {
			return;
		}

		stockMap.entrySet().stream().forEach(entry -> entry.setValue(true));

	}

	public boolean isInStock(String name) {
		return (name == null || name.isEmpty() || stockMap == null || stockMap.isEmpty()) ? 
				false : stockMap.get(name);
	}

	public void setInStock(String name, boolean inStock) {

		if(name == null || name.isEmpty() || stockMap == null || stockMap.isEmpty() ||
				!stockMap.containsKey(name)) {
			return;
		}

		stockMap.put(name, inStock);

	}

	@Override
	public void display() {

		super.display();

		if(entries == null || entries.isEmpty() || stockMap == null || stockMap.isEmpty()) {
			return;
		}

		final AtomicInteger counter = new AtomicInteger(0);
		entries.entrySet().stream().forEach(entry -> 
		output(counter.addAndGet(1) + DELIMITER + 
				entry.getKey() + DELIMITER + 
				StringUtil.DRINK_PRICE_FORMATTER.format(entry.getValue().getCost()) + 
				DELIMITER + stockMap.get(entry.getKey())));

	}

}
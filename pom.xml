<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<groupId>com.sundance.barista</groupId>
	<artifactId>barista-matic</artifactId>
	<version>1.0.0</version>
	<packaging>pom</packaging>

	<name>Barista-matic</name>

	<description>A simulator of an automatic coffee dispensing machine</description>

	<organization>
		<name>Sundance Studio</name>
		<url>https://www.linkedin.com/in/wei-zhou-2734ab38/</url>
	</organization>

	<developers>
		<developer>
			<id>wzhou</id>
			<name>Wei Zhou</name>
			<email>wzhoumadison@gmail.com</email>
			<organization>Sundance Studio</organization>
			<organizationUrl>https://www.linkedin.com/in/wei-zhou-2734ab38/</organizationUrl>
			<roles>
				<role>Architect</role>
				<role>Committer</role>
			</roles>
		</developer>
	</developers>

	<properties>

		<!-- Barista-matic version -->
		<barista-matic-version>1.0.0</barista-matic-version>

		<!-- Java version to target -->
		<project.build.javaVersion>1.8</project.build.javaVersion>

		<!-- Spring Framework Version -->
		<spring.version>5.0.6.RELEASE</spring.version>

		<!-- Maven plugin versions -->
		<maven-compiler-plugin-version>3.8.1</maven-compiler-plugin-version>
		<maven-jar-plugin-version>2.6</maven-jar-plugin-version>
		<maven-dependency-plugin-version>2.10</maven-dependency-plugin-version>
		<maven-surefire-plugin-version>2.19</maven-surefire-plugin-version>
		<maven-assembly-plugin-version>3.1.0</maven-assembly-plugin-version>
		<maven-shade-plugin-version>3.1.1</maven-shade-plugin-version>

		<!-- Other third-party dependency versions -->
		<org.apache.logging.log4j.version>2.11.1</org.apache.logging.log4j.version>
		<junit-version>4.12</junit-version>

		<!-- Flag of Unit Test -->
		<skipTests>false</skipTests>

		<!-- Others -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

	</properties>


	<dependencyManagement>

		<dependencies>


			<!-- Spring Framework Modules -->
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-context</artifactId>
				<version>${spring.version}</version>
			</dependency>

			<!-- Other Third Party Modules -->
			<dependency>
				<groupId>org.apache.logging.log4j</groupId>
				<artifactId>log4j-core</artifactId>
				<version>${org.apache.logging.log4j.version}</version>
			</dependency>

			<dependency>
				<groupId>junit</groupId>
				<artifactId>junit</artifactId>
				<version>${junit-version}</version>
			</dependency>

		</dependencies>

	</dependencyManagement>

	<build>
		<plugins>

			<!-- Set a compiler level -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>${maven-compiler-plugin-version}</version>
				<configuration>
					<source>${project.build.javaVersion}</source>
					<target>${project.build.javaVersion}</target>
					<encoding>${project.build.sourceEncoding}</encoding>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-shade-plugin</artifactId>
				<version>${maven-shade-plugin-version}</version>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>shade</goal>
						</goals>
						<configuration>
							<transformers>
								<transformer
									implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
									<resource>META-INF/spring.handlers</resource>
								</transformer>
								<transformer
									implementation="org.apache.maven.plugins.shade.resource.AppendingTransformer">
									<resource>META-INF/spring.schemas</resource>
								</transformer>
								<transformer
									implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
									<mainClass>${mainClass}</mainClass>
								</transformer>
							</transformers>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<!-- Copy project dependency -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${maven-surefire-plugin-version}</version>
				<configuration>
					<skipTests>${skipTests}</skipTests>
				</configuration>
			</plugin>

		</plugins>
	</build>

	<modules>
		<module>barista-matic-common</module>
		<module>barista-matic-main</module>
		<module>barista-matic-card</module>
		<module>barista-matic-map</module>
	</modules>

</project>